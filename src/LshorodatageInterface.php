<?php


namespace Libriciel\LshorodatageApiWrapper;


use Psr\Http\Message\StreamInterface;

interface LshorodatageInterface
{
    /**
     * @throws LsHorodatageException
     */
    public function setUrl(string $url): void;

    /**
     * @throws LsHorodatageException
     */
    public function ping(): string;

    /**
     * @throws LsHorodatageException
     */
    public function createTimestampToken(string $filePath): StreamInterface;

    /**
     * @throws LsHorodatageException
     */
    public function readTimestampToken(string $filePath): string;

    /**
     * @throws LsHorodatageException
     */
    public function verifyTimestampToken(string $filePath, string $tokenPath): bool;
}
