# LshorodatageApiWrapper

Wrapper php de l'api de lshorodatage.

### Prérequis
php >= 7.2

### Installation

```
composer require libriciel/lshorodatage-api-wrapper

```

### Initialisation
```

$lshorodatage = new Lshorodatage();
$lshorodatage->setUrl("http://lshorodatage:3000");

```

#### Verification de la connexion

```
$lshorodatage->ping();
```

#### Générer le token de timestamp d'un fichier

```
$lshorodatage->createTimestampToken("FILE/PATH");
```


#### Lire un token

```
$lshorodatage->readTimestampToken("FILE_TOKEN/PATH");
```


#### Vérifier la validité du token pour un fichier

```
$lshorodatage->verifyTimestampToken("FILE/PATH", "FILE_TOKEN/PATH");
```


#### Générer un timestamp via un service rgs

```
$lshorodatage->createRgsTimestampToken("FILE/PATH");
```
